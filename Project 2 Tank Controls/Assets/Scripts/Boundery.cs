﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundery : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerExit2D(Collider2D otherObject) // checks for collisions to the boundery
    {
        GameManager.instance.Enemies.Remove(otherObject.gameObject); // removes that object 
        Destroy(otherObject.gameObject); // destroys it
    }
}
