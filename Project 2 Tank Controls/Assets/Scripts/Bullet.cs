﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D laser; //sets the RigidBody as laser
    public float speed;// makes the speed public

    // Start is called before the first frame update
    void Start()
    {
        laser = GetComponent<Rigidbody2D>(); // makes sure laser is actually the rigid body
    }

    // Update is called once per frame
    void Update()
    {
        laser.velocity = transform.up * speed; // makes it move
    }

    void OnTriggerEnter2D(Collider2D other) // checks if something interacts with the trigger
    {
        if (other.CompareTag("EnemyShip") || other.gameObject.tag == "Meteor") // checks for certain tags
        {
            GameManager.instance.Enemies.Remove(gameObject); // removes object
            Destroy(other.gameObject); // destroys other object
            Destroy(gameObject); // destroys this object
        }
    }
}
