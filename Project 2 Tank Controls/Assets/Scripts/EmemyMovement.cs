﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EmemyMovement : MonoBehaviour
{
    private Rigidbody2D rb2d;//sets the RigidBody as rb2d
    public float speed; // makes the speed public
    private Vector3 faf; // creates a name from the vector3
    

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); // makes sure that it knows rb2d is getting the rigid body
        faf = (transform.position - GameManager.instance.PTransform.position).normalized; // sets faf up as the position of the player
        transform.up = faf; // sets the transform up as faf
        rb2d.velocity = -transform.up * speed; // makes it move
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    
    void OnCollisionEnter2D(Collision2D other) // checks if something collides with a meteor 
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "EnemyShip") // checks for the certain tags
        {
            GameManager.instance.Enemies.Remove(gameObject); // removes the object
            Destroy(other.gameObject); // destroys the other object 
        }

        if (other.gameObject.tag == "Player") // checks for certain tags
        {
            GameManager.instance.GameOver = !GameManager.instance.GameOver; // makes game over condition
        }
    }

}
  

