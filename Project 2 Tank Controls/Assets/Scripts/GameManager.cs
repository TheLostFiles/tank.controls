﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public List<GameObject> Enemies; 
    public static GameManager instance; 
    public Transform PTransform;
    public bool GameOver;
    public GameObject spawn;
    public GameObject HeatScript;
    public GameObject FireScript;
    public Text EndText;

    void Awake()
    {
        if (instance == null) // checks if the instance is null
        {
            instance = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one - this new object must die
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameOver) // checks if the game over condition 
        {
            End(); // runs the end function 
        }
    }

    void End() // 
    {
        EndText.text = "Game Over"; // outputs text

       

        for (int i = 0; i <= Enemies.Count; i++) // for loop
        { 
            Destroy(Enemies[i]); // destroys all enemies
        }
        Enemies.Clear(); // clears enemies

        spawn.GetComponent<Respawn>().enabled = false; // stops this script
        HeatScript.GetComponent<Respawn>().enabled = false; // stops this script
        FireScript.GetComponent<Respawn>().enabled = false; // stops this script

    }
}
