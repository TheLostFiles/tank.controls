﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HEnemyMovement : MonoBehaviour
{
    public float speed;// makes the speed public
    private Vector3 hs; // creates a name for the vector3

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        hs = (transform.position - GameManager.instance.PTransform.position).normalized; // sets up hs as the position if the player
        transform.up = hs; // sets the transform up to hs
        transform.position += -transform.up * speed; // makes the enemy move
    }

    void OnTriggerEnter2D(Collider2D other) // checks if things run into the trigger
    {
        if (other.CompareTag("Player")|| other.CompareTag("EnemyShip") || other.gameObject.tag == "Laser") // checks which tag they are under
        {
            GameManager.instance.Enemies.Remove(gameObject); // removes object
            Destroy(other.gameObject); // destroys the other object
            Destroy(gameObject); // destroys this object
        }
        if (other.CompareTag("Meteor")) // checks which tag they are under
        {
            GameManager.instance.Enemies.Remove(gameObject);// removes object
            Destroy(gameObject); // destroys this object
        }
        if (other.gameObject.tag == "Player") // checks which tag they are under
        {
            GameManager.instance.GameOver = !GameManager.instance.GameOver; // runs the game over condition
        }
    }
}
