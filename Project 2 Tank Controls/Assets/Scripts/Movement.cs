﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class Movement : MonoBehaviour
{
    private Rigidbody2D rb2d; //sets the RigidBody as rb2d
    public float speed; // makes the speed public
    public float rspeed; // makes the speed of the rotation public
    private static Quaternion rotation; // sets the Quaternion to rotation so that i can use the name
    public GameObject laser;
    private float nextlaser;
    public float firerate;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); // sets the name of the component so that I can use it easily 
        rotation = transform.rotation;// makes it so i can use the transform rotation easier
    }

    void Update()
    {

        if (Input.GetKey("w")) //grabs the w key
        {
            rb2d.velocity = transform.up * speed; // sets the movement of the rigidbody up so that it moves according to the speed
        }
        if (Input.GetKey("s"))//grabs the s key
        {
            rb2d.velocity = -transform.up * speed;// sets the movement of the rigidbody up so that it moves according to the speed
        }
        if (Input.GetKey("d"))//grabs the d key
        {
            transform.Rotate(new Vector3(0,0,-2) * Time.deltaTime * rspeed, Space.World); // makes the game object rotate according to the rotation speed
        }
        if (Input.GetKey("a"))//grabs the a key
        {
            transform.Rotate(new Vector3(0, 0, 2) * Time.deltaTime * rspeed, Space.World);// makes the game object rotate according to the rotation speed
        }
        if (Input.GetKey("space") && Time.time > nextlaser) // checks if you press space
        {
            nextlaser = Time.time + firerate; // makes sure the next shot can be shot 
            Instantiate(laser, GameManager.instance.PTransform.position + (transform.up * 1f), GameManager.instance.PTransform.rotation); // makes lasers+
        }
    }
}
