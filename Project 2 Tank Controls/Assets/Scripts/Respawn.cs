﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public List <Transform> SpawnList;
    public List <GameObject> EnemyList;
    public int MaxEnemies;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if(GameManager.instance.Enemies.Count < MaxEnemies) // checks if there are enough enemies 
        {
            int EType = Random.Range(0, MaxEnemies); // finds a random of the enemies
            int Spawn = Random.Range(0, SpawnList.Count); // picks a random spawn point 
            GameObject Enemy = Instantiate(EnemyList[EType], SpawnList[Spawn]); // spawns enemy
            GameManager.instance.Enemies.Add(Enemy); // adds enemy to the enemy amount
        }
    }
}
